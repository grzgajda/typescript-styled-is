import {
  FlattenInterpolation,
  ThemeProps,
  ThemedStyledProps,
} from "styled-components";

const mergeCss = (
  css: FlattenInterpolation<ThemeProps<any> | ThemedStyledProps<any, any>>,
  props: { [key: string]: any } = {}
): string =>
  css
    .reduce<string>(
      (a, b) => (b instanceof Function ? a + b(props) : a + b),
      ""
    )
    .split("\n")
    .map((n) => n.trim())
    .join(" ")
    .trim();

export default mergeCss;
