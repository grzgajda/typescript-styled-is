import is, { isNot } from "../index";
import mergeCss from "../mergeCss";

describe('function "is"', () => {
  it("should return styles when condition is met", () => {
    const result = is("outline")`display:none;`({ outline: true });
    const css = mergeCss(result);

    expect(css).toEqual("display:none;");
  });

  it("should return an empty string when condition is not met", () => {
    const result = is("outline")`display:none;`({ outline: false });
    const css = mergeCss(result);

    expect(css).toEqual("");
  });

  it("should allow to pass more than one prop as a condition", () => {
    const props = { outline: true, render: true };
    const result = is("outline", "render")`display:none;`(props);
    const css = mergeCss(result, props);

    expect(css).toEqual("display:none;");
  });

  it("should render styles only when all conditions are met", () => {
    const props = { outline: true, render: false };
    const result = is("outline", "render")`
      display:none;
    `(props);
    const css = mergeCss(result);

    expect(css).toEqual("");
  });

  it("should allow to use functions inside css block", () => {
    interface IProps {
      position: string;
    }

    const props = { outline: true, position: "relative" };
    const result = is("outline")`
      display: none; 
      position: ${({ position }: IProps) => position};
    `(props);

    expect(mergeCss(result, props)).toEqual(
      "display: none; position: relative;"
    );
  });

  it("should allow to nest conditions inside names with undefined props", () => {
    const props = { outline: true };
    const result = is("outline", isNot("primary"))`
      color: green;
    `(props);

    expect(mergeCss(result)).toEqual("color: green;");
  });

  it("should allow to nest conditions inside names", () => {
    const props = { outline: true, primary: false };
    const result = is("outline", isNot("primary"))`
      color: green;
    `(props);

    expect(mergeCss(result)).toEqual("color: green;");
  });

  it("should allow to check props values only without css", () => {
    const props = { outline: true, position: "relative" };
    const result = is("outline").valid(props);

    expect(result).toBeTruthy();
  });

  it("should allow to mix different conditions", () => {
    const props = { outline: true, primary: false };
    const result = is("outline", isNot("primary")).valid(props);

    expect(result).toBeTruthy();
  });

  it("should allow to use variables outside of scope as css (issue #3)", () => {
    const exampleTheme = { blue20: "blue20" };
    const result = is("outline")`
      color: ${exampleTheme.blue20};
    `({ outline: true });

    expect(mergeCss(result)).toEqual("color: blue20;");
  });
});
