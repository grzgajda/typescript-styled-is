import * as React from "react";
import { render } from "@testing-library/react";
import styled from "styled-components";
import "jest-styled-components";
import is, { isNot } from "..";

describe("real test with real React", () => {
  const Button = styled.button`
    color: red;
    ${is("primary")`
      color: green;
    `}
  `;

  it("should render button in DOM tree", () => {
    const { container } = render(<Button />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("should render button in DOM tree with primary prop", () => {
    const { container } = render(<Button primary={true} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});

describe("composition of function to check prop types", () => {
  const Button = styled.button`
    color: red;
    ${is("primary", isNot("secondary"))`
      color: green;
    `}
  `;

  it("should render button in DOM tree", () => {
    const { container } = render(<Button />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("should render with color green when is only primary", () => {
    const { container } = render(<Button primary={true} secondary={false} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("should render with color red when is also secondary", () => {
    const { container } = render(<Button primary={true} secondary={true} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});

describe("real test with typed button", () => {
  const Button = styled.button<{ primary?: boolean }>`
    color: red;
    ${is("primary")`
      color: green;
    `}
  `;
  it("should render button in DOM tree", () => {
    const { container } = render(<Button />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("should render button in DOM tree with primary prop", () => {
    const { container } = render(<Button primary={true} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
