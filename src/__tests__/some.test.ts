import { some, someNot } from "../index";
import mergeCss from "../mergeCss";

describe('function "some"', () => {
  it("should return styles when one of conditions is valid", () => {
    const props = { outline: true, display: false };
    const result = some("outline", "display")`display: block;`(props);
    const css = mergeCss(result);

    expect(css).toEqual("display: block;");
  });

  it("should return styles when there is only one condition and is valid", () => {
    const props = { outline: true, display: false };
    const result = some("outline")`display: block;`(props);
    const css = mergeCss(result);

    expect(css).toEqual("display: block;");
  });

  it("should return an empty string when all conditions are invalid", () => {
    const props = { outline: false, display: false };
    const result = some("outline")`display: block;`(props);
    const css = mergeCss(result);

    expect(css).toEqual("");
  });

  it("should allow to mix different conditions", () => {
    const props = { outline: true, primary: false };
    const result = some("outline", someNot("primary")).valid(props);

    expect(result).toBeTruthy();
  });
});
