import { someNot } from "../index";
import mergeCss from "../mergeCss";

describe('function "someNot"', () => {
  it("should return styles when one of conditons is invalid", () => {
    const props = { outline: true, display: false };
    const result = someNot("outline", "display")`display: block;`(props);
    const css = mergeCss(result);

    expect(css).toEqual("display: block;");
  });

  it("should return an empty string when all conditions are valid", () => {
    const props = { outline: true, display: true };
    const result = someNot("outline", "display")`display: block;`(props);
    const css = mergeCss(result);

    expect(css).toEqual("");
  });
});
