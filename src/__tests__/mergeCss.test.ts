import { css } from "styled-components";
import mergeCss from "../mergeCss";

describe('utility "mergeCss"', () => {
  it("should merge css definitions", () => {
    const example = css`
      display: none;
    `;
    expect(mergeCss(example)).toEqual("display: none;");
  });

  it("should allow to pass props inside", () => {
    const example = css`
      display: none;
      position: ${(props: { position: string }) => props.position};
    `;
    expect(mergeCss(example, { position: "static" })).toEqual(
      "display: none; position: static;"
    );
  });

  it("should trim text", () => {
    const example = css`
      display: none;
      position: absolute;
    `;
    expect(mergeCss(example)).toEqual("display: none; position: absolute;");
  });
});
