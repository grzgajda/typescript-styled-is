import { isNot } from "../index";
import mergeCss from "../mergeCss";

describe('function "isNot"', () => {
  it("should return an empty string when one of condition is not met", () => {
    const props = { outline: true };
    const result = isNot("outline")`display: block`(props);
    const css = mergeCss(result);

    expect(css).toEqual("");
  });

  it("should return styles when all conditions are false", () => {
    const props = { outline: false };
    const result = isNot("outline")`
      display: block;
    `(props);
    const css = mergeCss(result, props);

    expect(css).toEqual("display: block;");
  });

  it("should return styles when there is more than one conditon and all are met", () => {
    const props = { outline: false, display: false };
    const result = isNot("outline", "display")`
      display: block;
    `(props);
    const css = mergeCss(result, props);

    expect(css).toEqual("display: block;");
  });
});
