import {
  css,
  CSSObject,
  InterpolationFunction,
  ThemedStyledProps,
  FlattenInterpolation,
  StyledProps,
} from "styled-components";

type Style =
  | InterpolationFunction<ThemedStyledProps<any, any>>
  | TemplateStringsArray
  | CSSObject
  | string;

export interface StyleDefinition<P> {
  (...styles: Style[]): (
    props: Partial<P>
  ) => FlattenInterpolation<StyledProps<P>>;
  valid: (props: Partial<P>) => boolean;
}

const styledIs = (method: "some" | "every") => (condition: boolean) => <P, U>(
  ...names: Array<keyof P | StyleDefinition<U>>
) => {
  const isValid = (props: Partial<P & U>) =>
    names[method]((name) =>
      typeof name === "string"
        ? name in props
          ? Boolean((props as any)[name]) === condition
          : !condition
        : (name as StyleDefinition<Partial<P & U>>).valid(props)
    );

  const getCss = (...styles: Style[]) => (props: Partial<P & U>) => {
    const [style, ...rest] = styles;

    return isValid(props) ? css(style as any, ...rest) : css``;
  };

  getCss.valid = isValid;

  return getCss;
};

export default styledIs;
