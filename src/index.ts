import styledIs from "./styledIs";

const styledEvery = styledIs("every");
const is = styledEvery(true);
const isNot = styledEvery(false);

const styledSome = styledIs("some");
const some = styledSome(true);
const someNot = styledSome(false);

export default is;
export { is, isNot, some, someNot };
