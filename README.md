<div align="center">
    <img src="https://gitlab.com/grzgajda/typescript-styled-is/raw/master/logo.png" alt="typescript-styled-is" />
</div>

<div align="center">
    <strong>Flag utility for styled-components inspired with styled-is created by yldio. I missed support for TypeScript so I wrote my own package from scratch.</strong>
    <br />
    <br />
</div>

<div align="center">
  <img src="https://img.shields.io/badge/LANG-TypeScript-%232b7489.svg?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/pipeline/grzgajda/typescript-styled-is.svg?logo=gitlab&style=for-the-badge" />
  <img src="https://img.shields.io/badge/coverage-100%25-brightgreen.svg?logo=gitlab&style=for-the-badge" />
  <img src="https://img.shields.io/npm/v/typescript-styled-is.svg?logo=npm&style=for-the-badge" />
  <img src="https://img.shields.io/bundlephobia/minzip/typescript-styled-is.svg?style=for-the-badge" />
  <img src="https://img.shields.io/npm/dt/typescript-styled-is.svg?logo=npm&style=for-the-badge" />
  <img src="https://img.shields.io/badge/built%20with-styled%20components-%23c86a89.svg?style=for-the-badge" />
</div>

<br />
<br />

The purpose is to simplify writing and improve reading of conditional statements in _styled-components_. With this package it's possible to create advanced components (e.g. `<Button />` with different styles like _primary_, _secondary_) in one component without hussle.

## Installation

To use _typescript-styled-is_ there is required another package - _styled-components_. Otherwise package has no sense in your project. Depends of your package manager, you can use _yarn_ or _npm_.

```shell
yarn add typescript-styled-is
```

```shell
npm install typescript-styled-is
```

## Usage

Package _styled-components_ allows to conditional statements for CSS by comparing props' values. 

```tsx
import styled from 'styled-components'

const Button = styled.button`
  color: ${props => props.active ? 'active' : 'green'};
`
```

That syntax is not readable and nested conditions are even harder to read. _**typescript-styled-is**_ allows to easily write conditions.

```tsx
import styled from 'styled-components'
import is, { isNot, some, someNot } from 'typescript-styled-is'

const Button = styled.button`
  color: 'red';

  ${is('active')`
    color: 'green';
  `}

  ${isNot('primary', 'secondary')`
    color: 'blue';
  `}

  ${some('used', 'disabled', 'inactive')`
    color: 'violet';
  `}
  
  ${someNot('used', 'disabled', 'inactive')`
    color: 'violet';
  `}
`

<Button />
<Button active={true} primary={true} />
<Button used={true} />
```

This utility does not provide any tools to remove unused styles. Style `color: red` will be there always. So if your component is _active_, your styles will contain two declarations for color.

For more complicated scenarios, since version 2.0 it is possible to nest conditions, e.g.:

```tsx
import styled from 'styled-components'
import is, { isNot } from 'typescript-styled-is'

const Button = styled.button`
  color: red;
  ${is('primary, isNot('secondary'))`
    color: green;
  `}
`

<Button primary={true} />
<Button primary={true} secondary={false} />
```

## API

* `is(...props: string[])` returns css when all props are true _(default export)_
* `isNot(...props: string[])` return css when all props are false
* `some(...props: string[])` returns css when one of props is true
* `someNot(...props: string[])` returns css when one of props is false
